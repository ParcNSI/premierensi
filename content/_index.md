## HTML CSS

* HTML:
	* Documentation <http://w3schools.com/tags/> 
	* Validateur en ligne : <https://validator.w3.org/#validate_by_upload>
* CSS: 
	* Documentation <http://www.w3schools.com/css/>
	* Validateur en ligne : <https://validator.w3.org/unicorn/?ucn_lang=fr#validate-by-upload+task_conformance>

## Sites institutionnels

* Programme de première NSI:  <https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf>
* Sujet 0 de l'épreuve de contrôle continue de fin de première :
	* <https://cache.media.eduscol.education.fr/file/Annales_zero_BAC_2021_1e/87/9/S0BAC21-1e-SPE-NSI_1133879.pdf>
	* Sujet 0 qui avait été mis en ligne puis remplacé <https://framagit.org/flaustriat/nsi-1/blob/master/docs/sujet00.pdf>

## Jupyter Hub

Pour accéder aux Notebook Jupyter (fichiers d'extension ipynb dans les dossiers ressources ou corrige) du dépôt en mode interactif, cliquez sur le lien Binder ci-dessous.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ParcNSI%2Fpremierensi/master)

## Ressources Python :

* Installation de Python :
  - [Installation standard avec Idle](https://www.python.org/downloads/)
  - [Installation Anaconda + Pyzo](https://pyzo.org/start.html)
  - [Installation Thonny (debugger très pratique)](https://thonny.org/)

* Memento / Cheatsheet :
  - [Memento de Laurent Pointal](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf)
  
* Interpréteurs en ligne :
  - [repl.it](https://repl.it/languages/python3)
  - [Pythontutor](http://pythontutor.com/)  : permet de visualiser l'évolution des variables
  

## Conférence de Judicael Courant : "Une brève histoire des langages de programmation"

[Captation vidéo de la conférence](https://tube.ac-lyon.fr/videos/watch/2f7065e3-13c7-432c-80cc-94e769d38272)


## Chapitres 

### Chapitre  : Représentation des nombres

* [Cours version pdf](representation_nombres/Chapite6-ReprésentationNombres-2019V1.pdf)

### Chapitre : architecture, circuits logiques

- [cours version pdf](content/CircuitsLogiques/cours-circuits-logiques-.pdf)
- [cours version markdown](content/CircuitsLogiques/cours-circuits-logiques-git.md)
- [cours version  diaporama html](content/CircuitsLogiques/cours-circuits-logiques-slidy.html)
- [ressources](content/CircuitsLogiques)

